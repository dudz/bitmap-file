import math


class BitmapFile:

    __MM_POR_POL = 25.4
    __ESPESSURA_MAXIMA_MM = 3.0
    __ESPESSURA_PADRAO_MM = 0.25
    __ESPESSURA_MAXIMA = 30
    __ESPESSURA_PADRAO = 3
    __RESOLUCAO_MINIMA_DPI = 10.0
    __RESOLUCAO_MAXIMA_DPI = 2400.0
    __RESOLUCAO_PADRAO_PARA_MM = 300.0
    __RESOLUCAO_PADRAO = 72.0
    __DIMENSAO_MAXIMA_MM = (420, 297)
    __DIMENSAO_MAXIMA = (10000, 7500)
    __DIMENSAO_PADRAO = (800, 600)
    __DIMENSAO_PADRAO_MM = (210.0, 148.0)
    __DIMENSAO_MINIMA_MM = (10.0, 10.0)
    __PRETO = (0, 0, 0)
    __BRANCO = (255, 255, 255)
    __PALETA_PADRAO = (__BRANCO, __PRETO)

    def __init__(
        self,
        medidas_em_mm=False,
        dimensoes=None,
        resolucao_dpi=None,
        inverter_eixo_y=None,
        deslocamento_janela=(0, 0),
        espessura_linha=None,
        paleta=__PALETA_PADRAO,
        cor_fundo=None,
        cor_padrao=None
    ):
        self.__cor_padrao = None  # só para não dar warning
        self.__espessura_linha = None  # só para não dar warning
        self.__configura_paleta(paleta, cor_fundo, cor_padrao)
        self.__configura_medidas(medidas_em_mm, dimensoes, resolucao_dpi, inverter_eixo_y,
                                 deslocamento_janela, espessura_linha)
        self.__pontos_pintados = dict()

    # Ponto com coordenada de acordo com o tipo de imagem (mm ou pixels)
    def desenhar_ponto(self, ponto, cor=None, espessura=None):
        if not isinstance(ponto, tuple) or len(ponto) != 2:
            return
        if not isinstance(ponto[0], int) and not isinstance(ponto[0], float):
            return
        if not isinstance(ponto[1], int) and not isinstance(ponto[1], float):
            return
        x = ponto[0]
        y = ponto[1]
        if self.__medidas_em_mm:
            x = round(x * self.__resolucao_ppmm)
            y = round(y * self.__resolucao_ppmm)
        else:
            x = round(x)
            y = round(y)
        # Se o pixel central estiver fora da imagem, não desenha nada.
        if x < self.__deslocamento_janela[0] or x >= self.__limite_janela[0]:
            return
        if y < self.__deslocamento_janela[1] or y >= self.__limite_janela[1]:
            return
        centro = (x, y)
        if cor is not None:
            if not self.__cor_valida(cor):
                return
        else:
            cor = self.cor_padrao
        if espessura is None:
            espessura = self.espessura_linha
        else:
            if not self.__espessura_valida(espessura):
                espessura = self.espessura_linha
            else:
                if self.__medidas_em_mm:
                    espessura = round(espessura * self.__resolucao_ppmm)
            espessura = 1 + 2 * math.ceil((espessura - 1) / 2)
        chao_metade_espessura_linha = math.floor(espessura / 2)

        distancia_maxima = espessura / 2

        for k in range(-chao_metade_espessura_linha, chao_metade_espessura_linha + 1):
            for m in range(-chao_metade_espessura_linha, chao_metade_espessura_linha + 1):
                if BitmapFile.__distancia(centro, (x + k, y + m)) > distancia_maxima:
                    continue
                self.__desenhar_pixel((x + k, y + m), cor)

    def __desenhar_ponto(self, ponto, cor, espessura):
        # Uso interno sem validação. Atenção: Coordenada apenas em pixels.
        x = ponto[0]
        y = ponto[1]
        # Se o pixel central estiver fora da imagem, não desenha nada.
        if x < self.__deslocamento_janela[0] or x >= self.__limite_janela[0]:
            return
        if y < self.__deslocamento_janela[1] or y >= self.__limite_janela[1]:
            return
        centro = (x, y)
        chao_metade_espessura_linha = math.floor(espessura / 2)
        distancia_maxima = espessura / 2
        for k in range(-chao_metade_espessura_linha, chao_metade_espessura_linha + 1):
            for m in range(-chao_metade_espessura_linha, chao_metade_espessura_linha + 1):
                if BitmapFile.__distancia(centro, (x + k, y + m)) > distancia_maxima:
                    continue
                self.__desenhar_pixel((x + k, y + m), cor)

    def cor_pixel(self, ponto):
        # recupera cor do pixel
        # coordenadas apenas em pixels (pode ser não inteiro)
        if not isinstance(ponto, tuple) or len(ponto) != 2:
            return None
        if not isinstance(ponto[0], int) and not isinstance(ponto[0], float):
            return None
        if not isinstance(ponto[1], int) and not isinstance(ponto[1], float):
            return None
        x = round(ponto[0])
        y = round(ponto[1])
        if x < self.__deslocamento_janela[0] or x >= self.__limite_janela[0]:
            return None
        if y < self.__deslocamento_janela[1] or y >= self.__limite_janela[1]:
            return None
        ponto = (x, y)
        if ponto in self.__pontos_pintados.keys():
            return self.__pontos_pintados[ponto]
        return self.__cor_fundo

    def __desenhar_pixel(self, ponto, cor):
        # Uso interno sem validação. Coordenada do pixel na imagem (inteiros)
        # Se o pixel central estiver fora da imagem, não desenha nada.
        x = ponto[0]
        y = ponto[1]
        if x < self.__deslocamento_janela[0] or x >= self.__limite_janela[0]:
            return
        if y < self.__deslocamento_janela[1] or y >= self.__limite_janela[1]:
            return
        if cor != self.__cor_fundo:
            self.__pontos_pintados[ponto] = cor
        else:
            if ponto in self.__pontos_pintados.keys():
                del self.__pontos_pintados[ponto]

    def desenhar_pixel(self, ponto, cor=None):
        # Coordenada do pixel na imagem (pode ser não inteira, mas em pixels)
        if not isinstance(ponto, tuple) or len(ponto) != 2:
            return
        if not isinstance(ponto[0], int) and not isinstance(ponto[0], float):
            return
        if not isinstance(ponto[1], int) and not isinstance(ponto[1], float):
            return
        if cor is None:
            cor = self.cor_padrao
        else:
            if not self.__cor_valida(cor):
                return
        ponto = (round(ponto[0]), round(ponto[1]))
        self.__desenhar_pixel(ponto, cor)

    def gravar_arquivo(self, nome_arquivo, imprimir_percentual=False):
        if not isinstance(nome_arquivo, str) or len(nome_arquivo) < 1:
            return
        if nome_arquivo[-1] == '.':
            nome_arquivo = nome_arquivo[0:-1]
        if len(nome_arquivo) < 1:
            return
        if len(nome_arquivo) < 5 or str(nome_arquivo[-4:]).lower() != '.bmp':
            nome_arquivo += ".bmp"
        for ch in nome_arquivo:
            if ch not in ('.', '_', '-', '~', '!') and (
                    (ch < '0' or ch > '9')) and (
                    (ch < 'a' or ch > 'z')) and (
                    (ch < 'A' or ch > 'Z')):
                return
        if not isinstance(imprimir_percentual, bool):
            imprimir_percentual = False
        with open(nome_arquivo, "wb") as arquivo:
            self.__grava_header_arquivo(arquivo)
            self.__grava_header_bitmap(arquivo)
            self.__grava_paleta_cores(arquivo)
            if self.__bits_por_pixel == 1:
                self.__grava_pixels_imagem_1(arquivo, imprimir_percentual)
            elif self.__bits_por_pixel == 4:
                self.__grava_pixels_imagem_4(arquivo, imprimir_percentual)
            elif self.__bits_por_pixel == 8:
                self.__grava_pixels_imagem_8(arquivo, imprimir_percentual)
            elif self.__bits_por_pixel == 24:
                self.__grava_pixels_imagem_24(arquivo, imprimir_percentual)

    def __grava_header_arquivo(self, arq):
        # Bitmap file header
        arq.write(bytes("BM", "UTF-8"))
        tamanho_linha_bytes = 0
        if self.__bits_por_pixel != 24:
            tamanho_paleta_bytes = 4 * self.__qtd_cores
        else:
            tamanho_paleta_bytes = 0
        if self.__bits_por_pixel == 1:
            tamanho_pad = (32 - (self.__dimensoes[0] % 32)) % 32
            tamanho_linha_bytes = (self.__dimensoes[0] + tamanho_pad) // 8
        elif self.__bits_por_pixel == 4:
            tamanho_pad = (8 - (self.__dimensoes[0] % 8)) % 8
            tamanho_linha_bytes = (self.__dimensoes[0] + tamanho_pad) // 2
        elif self.__bits_por_pixel == 8:
            tamanho_pad = (4 - (self.__dimensoes[0] % 4)) % 4
            tamanho_linha_bytes = self.__dimensoes[0] + tamanho_pad
        elif self.__bits_por_pixel == 24:
            n_bytes = 3 * self.__dimensoes[0]
            tamanho_pad = (4 - (n_bytes % 4)) % 4
            tamanho_linha_bytes = n_bytes + tamanho_pad
        val = 14 + 40 + tamanho_paleta_bytes + self.__dimensoes[1] * tamanho_linha_bytes
        arq.write(val.to_bytes(4, "little"))  # tamanho arquivo
        arq.write(int(0).to_bytes(4, "little"))
        arq.write(int(14 + 40 + tamanho_paleta_bytes).to_bytes(4, "little"))  # deslocamento até dados

    def __grava_header_bitmap(self, arq):
        # BITMAPINFOHEADER
        arq.write(int(40).to_bytes(4, "little"))
        arq.write(self.__dimensoes[0].to_bytes(4, "little"))
        arq.write(self.__dimensoes[1].to_bytes(4, "little"))
        arq.write(int(1).to_bytes(2, "little"))  # color planes = 1
        arq.write(self.__bits_por_pixel.to_bytes(2, "little"))
        arq.write(int(0).to_bytes(4, "little"))  # compressao BI_RGB = 0
        arq.write(int(0).to_bytes(4, "little"))  # image size. Pode ser 0 para BI_RGB
        arq.write(round(self.__resolucao_ppmm * 1000).to_bytes(4, "little"))
        arq.write(round(self.__resolucao_ppmm * 1000).to_bytes(4, "little"))
        arq.write(int(0).to_bytes(4, "little"))  # cores na paleta
        arq.write(int(0).to_bytes(4, "little"))  # cores importantes

    def __grava_paleta_cores(self, arq):
        if self.__bits_por_pixel > 8:
            return
        for i in range(self.__qtd_cores):
            arq.write(int(self.__paleta[i][2]).to_bytes(1, "little"))
            arq.write(int(self.__paleta[i][1]).to_bytes(1, "little"))
            arq.write(int(self.__paleta[i][0]).to_bytes(1, "little"))
            arq.write(int(0).to_bytes(1, "little"))

    def __grava_pixels_imagem_1(self, arq, imprimir_percentual):
        if self.__inverter_eixo_y:
            fim = self.__deslocamento_janela[1] - 1
            inicio = fim + self.__dimensoes[1]
            passo = -1
        else:
            inicio = self.__deslocamento_janela[1]
            fim = inicio + self.__dimensoes[1]
            passo = 1
        tamanho_pad = (32 - (self.__dimensoes[0] % 32)) % 32
        cont_linhas = 0
        qtd_linhas = 0
        if imprimir_percentual:
            print("  0.00%", end="", flush=True)
            qtd_linhas = self.__dimensoes[1]
        for i in range(inicio, fim, passo):
            if imprimir_percentual:
                print(f"\r{round(100.0*cont_linhas / qtd_linhas,2):>6.2f}%", end="", flush=True)
                cont_linhas += 1
            bytes_linha = []
            c = 0
            count = 0
            for j in range(self.__deslocamento_janela[0], self.__deslocamento_janela[0] + self.__dimensoes[0]):
                count += 1
                c <<= 1
                if (j, i) not in self.__pontos_pintados:
                    c |= self.__cor_fundo
                else:
                    c |= self.__pontos_pintados[(j, i)]
                if count == 8:
                    bytes_linha.append(c)
                    count = 0
                    c = 0
            for j in range(0, tamanho_pad):
                count += 1
                c <<= 1
                if count == 8:
                    bytes_linha.append(c)
                    count = 0
                    c = 0
            arq.write(bytearray(bytes_linha))
        if imprimir_percentual:
            print("\r100.00%", flush=True)

    def __grava_pixels_imagem_4(self, arq, imprimir_percentual):
        if self.__inverter_eixo_y:
            fim = self.__deslocamento_janela[1] - 1
            inicio = fim + self.__dimensoes[1]
            passo = -1
        else:
            inicio = self.__deslocamento_janela[1]
            fim = inicio + self.__dimensoes[1]
            passo = 1
        tamanho_pad = (8 - (self.__dimensoes[0] % 8)) % 8
        cont_linhas = 0
        qtd_linhas = 0
        if imprimir_percentual:
            print("  0.00%", end="", flush=True)
            qtd_linhas = self.__dimensoes[1]
        for i in range(inicio, fim, passo):
            if imprimir_percentual:
                print(f"\r{round(100.0*cont_linhas / qtd_linhas, 2):>6.2f}%", end="", flush=True)
                cont_linhas += 1
            bytes_linha = []
            c = 0
            count = 0
            for j in range(self.__deslocamento_janela[0], self.__deslocamento_janela[0] + self.__dimensoes[0]):
                count += 1
                c <<= 4
                if (j, i) not in self.__pontos_pintados:
                    c |= self.__cor_fundo
                else:
                    c |= self.__pontos_pintados[(j, i)]
                if count == 2:
                    bytes_linha.append(c)
                    count = 0
                    c = 0
            for j in range(0, tamanho_pad):
                count += 1
                c <<= 4
                if count == 2:
                    bytes_linha.append(c)
                    count = 0
                    c = 0
            arq.write(bytearray(bytes_linha))
        if imprimir_percentual:
            print("\r100.00%", flush=True)

    def __grava_pixels_imagem_8(self, arq, imprimir_percentual):
        if self.__inverter_eixo_y:
            fim = self.__deslocamento_janela[1] - 1
            inicio = fim + self.__dimensoes[1]
            passo = -1
        else:
            inicio = self.__deslocamento_janela[1]
            fim = inicio + self.__dimensoes[1]
            passo = 1
        tamanho_pad = (4 - (self.__dimensoes[0] % 4)) % 4
        cont_linhas = 0
        qtd_linhas = 0
        if imprimir_percentual:
            print("  0.00%", end="", flush=True)
            qtd_linhas = self.__dimensoes[1]
        for i in range(inicio, fim, passo):
            if imprimir_percentual:
                print(f"\r{round(100.0*cont_linhas / qtd_linhas,2):>6.2f}%", end="", flush=True)
                cont_linhas += 1
            bytes_linha = []
            for j in range(self.__deslocamento_janela[0], self.__deslocamento_janela[0] + self.__dimensoes[0]):
                if (j, i) not in self.__pontos_pintados:
                    c = self.__cor_fundo
                else:
                    c = self.__pontos_pintados[(j, i)]
                bytes_linha.append(c)
            for j in range(0, tamanho_pad):
                bytes_linha.append(0)
            arq.write(bytearray(bytes_linha))
        if imprimir_percentual:
            print("\r100.00%", flush=True)

    def __grava_pixels_imagem_24(self, arq, imprimir_percentual):
        if self.__inverter_eixo_y:
            fim = self.__deslocamento_janela[1] - 1
            inicio = fim + self.__dimensoes[1]
            passo = -1
        else:
            inicio = self.__deslocamento_janela[1]
            fim = inicio + self.__dimensoes[1]
            passo = 1
        n_bytes = 3 * self.__dimensoes[0]
        tamanho_pad = (4 - (n_bytes % 4)) % 4
        cont_linhas = 0
        qtd_linhas = 0
        if imprimir_percentual:
            print("  0.00%", end="", flush=True)
            qtd_linhas = self.__dimensoes[1]
        for i in range(inicio, fim, passo):
            if imprimir_percentual:
                print(f"\r{round(100.0*cont_linhas / qtd_linhas,2):>6.2f}%", end="", flush=True)
                cont_linhas += 1
            bytes_linha = []
            for j in range(self.__deslocamento_janela[0], self.__deslocamento_janela[0] + self.__dimensoes[0]):
                if (j, i) not in self.__pontos_pintados:
                    c = self.__cor_fundo
                else:
                    c = self.__pontos_pintados[(j, i)]
                bytes_linha.append(c[2])
                bytes_linha.append(c[1])
                bytes_linha.append(c[0])
            for j in range(0, tamanho_pad):
                bytes_linha.append(0)
            arq.write(bytearray(bytes_linha))
        if imprimir_percentual:
            print("\r100.00%", flush=True)

    def __configura_paleta(self, paleta, cor_fundo, cor_padrao):
        if paleta is not None:
            if not isinstance(paleta, tuple):
                paleta = BitmapFile.__PALETA_PADRAO
            qtd_cores = len(paleta)
            if qtd_cores <= 2:
                self.__bits_por_pixel = 1
            elif qtd_cores <= 16:
                self.__bits_por_pixel = 4
            elif qtd_cores <= 256:
                self.__bits_por_pixel = 8
            else:
                paleta = BitmapFile.__PALETA_PADRAO
                qtd_cores = 2
                self.__bits_por_pixel = 1
            paleta_completa = list(paleta)
            # completar a paleta até a quantidade correta de cores.
            for i in range(len(paleta_completa), 2 ** self.__bits_por_pixel):
                paleta_completa.append((0, 0, 0))
            paleta = tuple(paleta_completa)
            for i in range(qtd_cores):
                if not isinstance(paleta[i], tuple) or len(paleta[i]) != 3:
                    paleta = BitmapFile.__PALETA_PADRAO
                    qtd_cores = 2
                    self.__bits_por_pixel = 1
                    break
                for j in range(3):
                    if not isinstance(paleta[i][j], int) or paleta[i][j] < 0 or paleta[i][j] > 255:
                        paleta = BitmapFile.__PALETA_PADRAO
                        qtd_cores = 2
                        self.__bits_por_pixel = 1
                        break
                if paleta == BitmapFile.__PALETA_PADRAO:
                    break
            self.__paleta = paleta
            if not isinstance(cor_fundo, int) or cor_fundo < 0 or cor_fundo >= qtd_cores:
                cor_fundo = 0
            self.__cor_fundo = cor_fundo
        else:
            self.__bits_por_pixel = 24
            self.__paleta = None
            if not isinstance(cor_fundo, tuple) or len(cor_fundo) != 3:
                cor_fundo = BitmapFile.__BRANCO
            for i in range(3):
                if not isinstance(cor_fundo[i], int) or cor_fundo[i] < 0 or cor_fundo[i] > 255:
                    cor_fundo = BitmapFile.__BRANCO
                    break
            self.__cor_fundo = cor_fundo
        self.__qtd_cores = 2 ** self.__bits_por_pixel
        self.cor_padrao = cor_padrao

    def __configura_medidas(self, medidas_em_mm, dimensoes, resolucao_dpi, inverter_eixo_y,
                            deslocamento_janela, espessura_linha):
        if not isinstance(medidas_em_mm, bool):
            medidas_em_mm = False
        self.__medidas_em_mm = medidas_em_mm
        if self.__medidas_em_mm:
            if not isinstance(resolucao_dpi, int) and not isinstance(resolucao_dpi, float):
                resolucao_dpi = BitmapFile.__RESOLUCAO_PADRAO_PARA_MM
            resolucao_dpi = float(resolucao_dpi)
            if resolucao_dpi < BitmapFile.__RESOLUCAO_MINIMA_DPI or resolucao_dpi > BitmapFile.__RESOLUCAO_MAXIMA_DPI:
                resolucao_dpi = BitmapFile.__RESOLUCAO_PADRAO_PARA_MM
            self.__resolucao_dpi = resolucao_dpi
            self.__resolucao_ppmm = self.__resolucao_dpi / BitmapFile.__MM_POR_POL
            if not isinstance(dimensoes, tuple) or len(dimensoes) != 2 or (
                    not isinstance(dimensoes[0], int) and not isinstance(dimensoes[0], float)) or (
                    dimensoes[0] < BitmapFile.__DIMENSAO_MINIMA_MM[0]) or (
                    dimensoes[0] > BitmapFile.__DIMENSAO_MAXIMA_MM[0]) or (
                    not isinstance(dimensoes[1], int) and not isinstance(dimensoes[1], float)) or (
                    dimensoes[1] < BitmapFile.__DIMENSAO_MINIMA_MM[1]) or (
                    dimensoes[1] > BitmapFile.__DIMENSAO_MAXIMA_MM[1]):
                dimensoes = BitmapFile.__DIMENSAO_PADRAO_MM
            dimensao_x = round(dimensoes[0] * self.__resolucao_ppmm)
            dimensao_y = round(dimensoes[1] * self.__resolucao_ppmm)
            self.__dimensoes = (dimensao_x, dimensao_y)
            if not isinstance(inverter_eixo_y, bool):
                inverter_eixo_y = False
            self.__inverter_eixo_y = inverter_eixo_y
            if not isinstance(deslocamento_janela, tuple) or len(deslocamento_janela) != 2 or (
                not isinstance(deslocamento_janela[0], int) and not isinstance(deslocamento_janela[0], float)) or (
                    not isinstance(deslocamento_janela[1], int) and not isinstance(deslocamento_janela[1], float)):
                deslocamento_janela = (0.0, 0.0)
            deslocamento_x = round(deslocamento_janela[0] * self.__resolucao_ppmm)
            deslocamento_y = round(deslocamento_janela[1] * self.__resolucao_ppmm)
            self.__deslocamento_janela = (deslocamento_x, deslocamento_y)
        else:
            if not isinstance(dimensoes, tuple) or len(dimensoes) != 2 or (
                    not isinstance(dimensoes[0], int)) or dimensoes[0] <= 0 or (
                    dimensoes[0] > BitmapFile.__DIMENSAO_MAXIMA[0]) or (
                    not isinstance(dimensoes[1], int)) or dimensoes[1] <= 0 or (
                    dimensoes[1] > BitmapFile.__DIMENSAO_MAXIMA[1]):
                dimensoes = BitmapFile.__DIMENSAO_PADRAO
            self.__dimensoes = dimensoes
            if not isinstance(resolucao_dpi, int) and not isinstance(resolucao_dpi, float):
                resolucao_dpi = BitmapFile.__RESOLUCAO_PADRAO
            resolucao_dpi = float(resolucao_dpi)
            if resolucao_dpi < BitmapFile.__RESOLUCAO_MINIMA_DPI or resolucao_dpi > BitmapFile.__RESOLUCAO_MAXIMA_DPI:
                resolucao_dpi = BitmapFile.__RESOLUCAO_PADRAO
            self.__resolucao_dpi = resolucao_dpi
            self.__resolucao_ppmm = self.__resolucao_dpi / BitmapFile.__MM_POR_POL
            if not isinstance(inverter_eixo_y, bool):
                inverter_eixo_y = True
            self.__inverter_eixo_y = inverter_eixo_y
            if not isinstance(deslocamento_janela, tuple) or len(deslocamento_janela) != 2 or (
                    not isinstance(deslocamento_janela[0], int)) or not isinstance(deslocamento_janela[1], int):
                deslocamento_janela = (0, 0)
            self.__deslocamento_janela = deslocamento_janela
        self.__limite_janela = (self.__dimensoes[0] + self.__deslocamento_janela[0],
                                self.__dimensoes[1] + self.__deslocamento_janela[1])
        self.espessura_linha = espessura_linha

    @property
    def espessura_linha(self):
        return self.__espessura_linha

    @espessura_linha.setter
    def espessura_linha(self, espessura_linha):
        if not self.__espessura_valida(espessura_linha):
            if self.__medidas_em_mm:
                espessura_linha = BitmapFile.__ESPESSURA_PADRAO_MM
                espessura_linha = round(espessura_linha * self.__resolucao_ppmm)
            else:
                espessura_linha = BitmapFile.__ESPESSURA_PADRAO
        else:
            if self.__medidas_em_mm:
                espessura_linha = round(espessura_linha * self.__resolucao_ppmm)
        self.__espessura_linha = 1 + 2 * math.ceil((espessura_linha - 1) / 2)

    def __espessura_valida(self, espessura_linha):
        if self.__medidas_em_mm:
            if (not isinstance(espessura_linha, int) and not isinstance(espessura_linha, float)) or (
                    espessura_linha < 0.0) or espessura_linha > BitmapFile.__ESPESSURA_MAXIMA_MM:
                return False
        else:
            if not isinstance(espessura_linha, int) or espessura_linha < 1 or (
                    espessura_linha > BitmapFile.__ESPESSURA_MAXIMA):
                return False
        return True

    @property
    def cor_padrao(self):
        return self.__cor_padrao

    @cor_padrao.setter
    def cor_padrao(self, cor_padrao):
        if not self.__cor_valida(cor_padrao):
            cor_padrao = 1 if self.__bits_por_pixel < 24 else (0, 0, 0)
        self.__cor_padrao = cor_padrao

    def __cor_valida(self, cor):
        if self.__bits_por_pixel < 24:
            if not isinstance(cor, int) or cor < 0 or cor >= self.__qtd_cores:
                return False
        else:
            if not isinstance(cor, tuple) or len(cor) != 3:
                return False
            for i in range(3):
                if not isinstance(cor[i], int) or cor[i] < 0 or cor[i] > 255:
                    return False
        return True

    # Devolve a distância entre dois pixels na imagem
    @staticmethod
    def __distancia(x, y):
        """
        x0 = x[0]
        x1 = x[1]
        y0 = y[0]
        y1 = y[1]
        deltax = abs(x[0] - y[0])
        deltay = abs(x[1] - y[1])
        q1 = math.pow(deltax, 2.0)
        q2 = math.pow(deltay, 2.0)
        soma = q1 + q2
        sq = math.sqrt(soma)
        return sq
        """
        return math.sqrt(math.pow(abs(x[0] - y[0]), 2.0) + math.pow(abs(x[1] - y[1]), 2.0))

    # Uso interno, sem conferência. Coordenadas só em pixels
    def __desenhar_linha(self, origem, destino, cor, espessura):
        x0 = origem[0]
        y0 = origem[1]
        x1 = destino[0]
        y1 = destino[1]
        if x0 > x1:  # garante desenho da esquerda para direita
            x0, x1 = x1, x0
            y0, y1 = y1, y0
        inverte_y = True if y1 < y0 else False  # tratar y sempre crescente, mas marcar que deve subtrair se for o caso
        delta_x = x1 - x0
        delta_y = abs(y1 - y0)
        inverte_xy = False if delta_x >= delta_y else True
        if inverte_xy:  # garante que eixo x varie mais que y no cálculo da linha
            x0, y0 = y0, x0
            x1, y1 = y1, x1
            delta_x, delta_y = delta_y, delta_x
            if x0 > x1:
                x0, x1 = x1, x0
                y0, y1 = y1, y0
        # Algoritmo de Bresenham
        dois_dx = 2 * delta_x
        dois_dy = 2 * delta_y
        y = y0
        d = - delta_x
        for x in range(x0, x1 + 1):
            if inverte_xy:
                py = x
                if inverte_y:
                    px = 2 * y0 - y
                else:
                    px = y
            else:
                px = x
                if inverte_y:
                    py = 2 * y0 - y
                else:
                    py = y
            self.__desenhar_ponto((px, py), cor, espessura)
            d += dois_dy
            if d >= 0:
                y += 1
                d -= dois_dx

    def desenhar_linha(self, origem, destino, cor=None, espessura=None):
        if not isinstance(origem, tuple) or len(origem) != 2:
            return
        if not isinstance(origem, tuple) or len(origem) != 2:
            return
        if not isinstance(origem[0], int) and not isinstance(origem[0], float):
            return
        if not isinstance(origem[1], int) and not isinstance(origem[1], float):
            return
        if not isinstance(destino[0], int) and not isinstance(destino[0], float):
            return
        if not isinstance(destino[1], int) and not isinstance(destino[1], float):
            return
        x0 = origem[0]
        y0 = origem[1]
        x1 = destino[0]
        y1 = destino[1]
        if self.__medidas_em_mm:
            x0 = round(x0 * self.__resolucao_ppmm)
            y0 = round(y0 * self.__resolucao_ppmm)
            x1 = round(x1 * self.__resolucao_ppmm)
            y1 = round(y1 * self.__resolucao_ppmm)
        else:
            x0 = round(x0)
            y0 = round(y0)
            x1 = round(x1)
            y1 = round(y1)
        if cor is not None:
            if not self.__cor_valida(cor):
                return
        else:
            cor = self.cor_padrao
        if espessura is None:
            espessura = self.espessura_linha
        else:
            if not self.__espessura_valida(espessura):
                espessura = self.espessura_linha
            else:
                if self.__medidas_em_mm:
                    espessura = round(espessura * self.__resolucao_ppmm)
            espessura = 1 + 2 * math.ceil((espessura - 1) / 2)
        self.__desenhar_linha(origem=(x0, y0), destino=(x1, y1), cor=cor, espessura=espessura)

    def desenhar_retangulo(self, vertice1, vertice2, cor=None, espessura=None):
        if not isinstance(vertice1, tuple) or len(vertice1) != 2:
            return
        if not isinstance(vertice1, tuple) or len(vertice1) != 2:
            return
        if not isinstance(vertice1[0], int) and not isinstance(vertice1[0], float):
            return
        if not isinstance(vertice1[1], int) and not isinstance(vertice1[1], float):
            return
        if not isinstance(vertice2[0], int) and not isinstance(vertice2[0], float):
            return
        if not isinstance(vertice2[1], int) and not isinstance(vertice2[1], float):
            return
        x0 = vertice1[0]
        y0 = vertice1[1]
        x1 = vertice2[0]
        y1 = vertice2[1]
        if self.__medidas_em_mm:
            x0 = round(x0 * self.__resolucao_ppmm)
            y0 = round(y0 * self.__resolucao_ppmm)
            x1 = round(x1 * self.__resolucao_ppmm)
            y1 = round(y1 * self.__resolucao_ppmm)
        else:
            x0 = round(x0)
            y0 = round(y0)
            x1 = round(x1)
            y1 = round(y1)
        if cor is not None:
            if not self.__cor_valida(cor):
                return
        else:
            cor = self.cor_padrao
        if espessura is None:
            espessura = self.espessura_linha
        else:
            if not self.__espessura_valida(espessura):
                espessura = self.espessura_linha
            else:
                if self.__medidas_em_mm:
                    espessura = round(espessura * self.__resolucao_ppmm)
            espessura = 1 + 2 * math.ceil((espessura - 1) / 2)
        self.__desenhar_linha(origem=(x0, y0), destino=(x1, y0), cor=cor, espessura=espessura)
        self.__desenhar_linha(origem=(x1, y0), destino=(x1, y1), cor=cor, espessura=espessura)
        self.__desenhar_linha(origem=(x1, y1), destino=(x0, y1), cor=cor, espessura=espessura)
        self.__desenhar_linha(origem=(x0, y1), destino=(x0, y0), cor=cor, espessura=espessura)

    def desenhar_arco(self, centro, raio, angulo_ini=None, angulo_fim=None, cor=None, espessura=None):
        if not isinstance(centro, tuple) or len(centro) != 2:
            return
        if not isinstance(raio, int) and not isinstance(raio, float):
            return
        if not isinstance(centro[0], int) and not isinstance(centro[0], float):
            return
        if not isinstance(centro[1], int) and not isinstance(centro[1], float):
            return
        if round(raio) <= 0:
            return
        if angulo_ini is None:
            angulo_ini = 0.0
        if not isinstance(angulo_ini, int) and not isinstance(angulo_ini, float):
            return
        if angulo_fim is None:
            angulo_fim = 360.0
        if not isinstance(angulo_fim, int) and not isinstance(angulo_fim, float):
            return
        x0 = centro[0]
        y0 = centro[1]
        if self.__medidas_em_mm:
            x0 = round(x0 * self.__resolucao_ppmm)
            y0 = round(y0 * self.__resolucao_ppmm)
            raio = raio * self.__resolucao_ppmm
        else:
            x0 = round(x0)
            y0 = round(y0)
        if cor is not None:
            if not self.__cor_valida(cor):
                return
        else:
            cor = self.cor_padrao
        if espessura is None:
            espessura = self.espessura_linha
        else:
            if not self.__espessura_valida(espessura):
                espessura = self.espessura_linha
            else:
                if self.__medidas_em_mm:
                    espessura = round(espessura * self.__resolucao_ppmm)
            espessura = 1 + 2 * math.ceil((espessura - 1) / 2)
        ang = BitmapFile.__normaliza_angulo_graus(angulo_ini)
        angulo_fim = BitmapFile.__normaliza_angulo_graus(angulo_fim)
        if angulo_fim == ang:
            ang = 0
            angulo_fim = 0
            volta_completa = True
        else:
            volta_completa = False
        ang = math.radians(ang)
        angulo_fim = math.radians(angulo_fim)
        passo = math.atan2(0.5, raio)
        if volta_completa:
            angulo_fim = 2.0 * math.pi
        while ang <= angulo_fim:
            x = x0 + round(math.cos(ang) * raio)
            y = y0 + round(math.sin(ang) * raio)
            self.__desenhar_ponto(ponto=(x, y), cor=cor, espessura=espessura)
            ang += passo

    @staticmethod
    def __normaliza_angulo_graus(ang):
        d = 360.0
        while ang < 0.0:
            ang += d
            d *= 2
        d = 360.0
        while ang >= 360.0:
            if ang - d < 0:
                d /= 2
            else:
                ang -= d
                if ang > d:
                    d *= 2
        return ang


def main():
    x = BitmapFile(medidas_em_mm=True, dimensoes=(100, 100), espessura_linha=3, resolucao_dpi=1200)
    x.desenhar_ponto((50, 50))
    x.desenhar_linha(origem=(10, 20), destino=(90, 90), espessura=1)
    x.espessura_linha = None
    for i in range(0, 16):
        ang = 360 * i / 16 * math.pi / 180
        dx = 20 * math.cos(ang)
        dy = 20 * math.sin(ang)
        x.desenhar_linha((25, 75), (25+dx, 75+dy))
    x.desenhar_retangulo((30, 30), (70, 70))
    x.desenhar_arco((60, 60), 20, 10, 100)
    x.desenhar_arco((60, 60), 30)
    x.gravar_arquivo("teste.bmp", True)


if __name__ == "__main__":
    main()
