"""
Desenha espiral áurea
"""

import math
import sys
from bitmap_file import BitmapFile

# Constantes matemáticas
PHI = (1.0 + math.sqrt(5)) / 2.0
MM_POR_POL = 25.4
DOIS_SOBRE_PI = 2.0 / math.pi

# Parâmetros do desenho
ESPESSURA_LINHA_MM = 0.25

DPI = 1200
DIM_X_MM = 297 - (2 * 19)  # margens de 19mm
DIM_Y_MM = 210 - (2 * 19)

# Parâmetros derivados
DIM_X_MM_SOBRE_2 = DIM_X_MM / 2
DIM_Y_MM_SOBRE_2 = DIM_Y_MM / 2

def main():
    angulo_min = math.pi / 2 * math.log(ESPESSURA_LINHA_MM / 2) / math.log(PHI)
    tamanho_diagonal = math.sqrt(math.pow(DIM_X_MM_SOBRE_2, 2) + math.pow(DIM_Y_MM_SOBRE_2, 2))
    angulo_max = math.pi / 2 * math.log(tamanho_diagonal) / math.log(PHI)

    img = BitmapFile(medidas_em_mm=True,dimensoes=(DIM_X_MM,DIM_Y_MM),resolucao_dpi=DPI,
                     deslocamento_janela=(- DIM_X_MM_SOBRE_2, - DIM_Y_MM_SOBRE_2),
                     paleta=((255,255,255),(0,0,255)),
                     cor_padrao=1,
                     espessura_linha=ESPESSURA_LINHA_MM)

    print("Gerando imagem...", flush=True, end="")
    percentual_ant = 0
    angulo = angulo_min
    while angulo <= angulo_max:
        percentual = round(100 * (angulo - angulo_min) / (angulo_max - angulo_min))
        if percentual != percentual_ant:
            percentual_ant = percentual
            print(f"\rGerando imagem... {percentual}%", flush=True, end="")
        raio = math.pow(PHI, angulo * DOIS_SOBRE_PI)
        x = raio * math.cos(angulo)
        y = raio * math.sin(angulo)
        img.desenhar_ponto((x,y))
        passo = math.atan2(0.5, raio / MM_POR_POL * DPI)
        angulo += passo
    print(f"\rGerando imagem... feito.")
    print("Gravando arquivo")
    img.gravar_arquivo("espiral.bmp",True)

if __name__ == "__main__":
    main()
